//! This crate provides a macro (`async_sync_trait`) to define async traits (and their
//! implementations) while automatically providing synchronous versions of each method named
//! `method_name_sync`. See the macro's docs for details.

#![forbid(unsafe_code)]

extern crate proc_macro;

mod args;

use args::Args;
use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::{format_ident, quote, ToTokens};
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input, parse_quote,
    punctuated::Punctuated,
    token::Comma,
    Attribute, Error, Expr, FnArg, Ident, ImplItem, ItemImpl, ItemTrait, Result, Token, TraitItem,
};

/// This macro can be applied to `trait`s and `impl`s of those traits to automatically define
/// `_sync` versions of async functions.
///
/// Under the hood, it delegates to `async_trait` for the actual async trait functionality
/// (which should eventually work without an extra crate). It provides sync functionality by
/// introducing a `get_runtime()` method that the user must implement and which returns
/// a `&mut tokio::runtime::Runtime`. Then, it defines non-`async` methods named `method_name_sync`
/// with otherwise identical signatures.
///
/// When applied to a `impl` block, the macro also delegates to `async_trait` for the async
/// functions. It fills in the implementations for the `_sync` functions by using the
/// `get_runtime()` method to obtain a runtime to call `block_on` on, and then delegating to
/// the async version.
///
/// Note that this bug causes errors inside the trait/impl to not have meaningful line numbers:
/// https://github.com/rust-lang/rust/issues/68430
#[proc_macro_attribute]
pub fn async_sync_trait(args: TokenStream, input: TokenStream) -> TokenStream {
    let args = parse_macro_input!(args as Args);
    let mut item = parse_macro_input!(input as Item);
    expand(&mut item, args.local);
    if args.local {
        TokenStream::from(quote! {
            #[async_trait::async_trait(?Send)]
            #item
        })
    } else {
        TokenStream::from(quote! {
            #[async_trait::async_trait]
            #item
        })
    }
}

/// Used to specify that a method inside of an `async_sync_trait` trait or impl should *not*
/// produce a `_sync` version. Does not modify whatever it's applied to.
#[proc_macro_attribute]
pub fn no_sync(_args: TokenStream, input: TokenStream) -> TokenStream {
    input
}

fn expand(input: &mut Item, _is_local: bool) {
    match input {
        Item::Trait(input) => {
            // Generate non-async versions of all methods, with `_sync` appended. Also add a
            // `get_runtime()` method that the implementation will use to obtain a `&mut` ref to
            // a tokio runtime
            let get_runtime = parse_quote! {
                /// Obtain a reference to a runtime
                fn get_runtime(&self) -> Box<dyn DerefMut<Target = Runtime> + '_>;
            };
            input.items.push(get_runtime);
            let mut new_methods = vec![];
            for inner in &mut input.items {
                if let TraitItem::Method(method) = inner {
                    // Ignore methods marked with #[no_sync]
                    if method.attrs.contains(&parse_quote!(#[no_sync])) {
                        continue;
                    }
                    // Create the `_sync` version if it's async.
                    if method.sig.asyncness.is_some() {
                        let mut syncified = method.clone();
                        syncified.sig.asyncness = None;
                        let old_id = syncified.sig.ident.to_string();
                        syncified.sig.ident =
                            Ident::new(&format!("{}_sync", old_id), syncified.sig.ident.span());
                        new_methods.push(TraitItem::Method(syncified));
                    }
                }
            }
            input.items.extend(new_methods.into_iter());
        }
        Item::Impl(input) => {
            let mut new_methods = vec![];
            // Create the `_sync` versions of all non-#[no_sync] functions
            for inner in &mut input.items {
                if let ImplItem::Method(method) = inner {
                    if method.attrs.contains(&parse_quote!(#[no_sync])) {
                        continue;
                    }
                    if method.sig.asyncness.is_some() {
                        let mut syncified = method.clone();
                        syncified.sig.asyncness = None;
                        let old_id = syncified.sig.ident.clone();
                        let new_id = format_ident!("{}_sync", old_id);
                        syncified.sig.ident = new_id;
                        let args = syncified.sig.inputs.clone();
                        // Skip the self arg
                        let passed_args: Punctuated<Expr, Comma> = args
                            .into_pairs()
                            // TODO: This will definitely break on some odd `self` types
                            .filter(|pair| pair.value() != &parse_quote!(&self)
                                && pair.value() != &parse_quote!(self))
                            .map(|pair| match pair.value() {
                                FnArg::Typed(t) => {
                                    // Convert the pattern for the function signature argument
                                    // into an expression which is just the name binding without
                                    // the type parameter.
                                    let pat = &t.pat;
                                    let exp: Expr = parse_quote!(#pat);
                                    exp
                                }
                                FnArg::Receiver(r) =>
                                    panic!("There should not be a self type here: {:?}", r),
                            })
                            .collect();
                        // Replace body with delegation to async version
                        syncified.block = parse_quote! {
                            {
                                self.get_runtime().block_on(self.#old_id(#passed_args))
                            }
                        };
                        new_methods.push(ImplItem::Method(syncified));
                    }
                }
            }
            input.items.extend(new_methods.into_iter());
        }
    }
}

// This is lifted from async-trait, and determines if the attribute was applied to a trait or impl
enum Item {
    Trait(ItemTrait),
    Impl(ItemImpl),
}

impl ToTokens for Item {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        match self {
            Item::Trait(item) => item.to_tokens(tokens),
            Item::Impl(item) => item.to_tokens(tokens),
        }
    }
}

impl Parse for Item {
    fn parse(input: ParseStream) -> Result<Self> {
        let attrs = input.call(Attribute::parse_outer)?;
        let mut lookahead = input.lookahead1();
        if lookahead.peek(Token![unsafe]) {
            let ahead = input.fork();
            ahead.parse::<Token![unsafe]>()?;
            lookahead = ahead.lookahead1();
        }
        if lookahead.peek(Token![pub]) || lookahead.peek(Token![trait]) {
            let mut item: ItemTrait = input.parse()?;
            item.attrs = attrs;
            Ok(Item::Trait(item))
        } else if lookahead.peek(Token![impl]) {
            let mut item: ItemImpl = input.parse()?;
            if item.trait_.is_none() {
                return Err(Error::new(Span::call_site(), "expected a trait impl"));
            }
            item.attrs = attrs;
            Ok(Item::Impl(item))
        } else {
            Err(lookahead.error())
        }
    }
}
